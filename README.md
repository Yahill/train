# trains
To start the project use command go run main.go run.

In config.yaml file you can set up port for the application. By default it is
8080 You can set it up only from 8000 to 8500.

Method GET http://localhost:8080/routes/trains gives the shortest route
for each train.

Method http://localhost:8080/routes/trains/id{int} gives the shortest route for
train with this id.

Information about trains is taking from attached .xml file.
