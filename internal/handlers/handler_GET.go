package handlers

import (
	"net/http"
	"encoding/json"
	"github.com/spf13/train/internal/train"
	"github.com/spf13/train/internal/logger"
	"github.com/gorilla/mux"
)

func GetRoutesForTrains(w http.ResponseWriter, r *http.Request){
	tr := train.LoadTrains()
	tr.AllStations()

	logger.Info("Someone asked for the routes.")

	w.WriteHeader(http.StatusOK)

	//encode to JSON ans send it
	json.NewEncoder(w).Encode(tr)
}

func GerTrainByID(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	id := vars["id"]

	tr := train.LoadTrains()
	tr.AllStations()
	trainWithID, err := tr.GetTrainByID(id)
	if err != nil{
		w.WriteHeader(http.StatusNotFound)
	} else {
		logger.Info("Someone asked for train by ID.")

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(trainWithID)
	}
}