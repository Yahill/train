package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/train/internal/config"
	"github.com/gorilla/mux"
	"github.com/spf13/train/internal/handlers"
	"log"
	"net/http"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "This command starts server.",
	Long: `Settings in config.yaml file, 
where you can choose dataset and port for server.`,
	Run: func(cmd *cobra.Command, args []string) {

		//read config.yaml file with server settings
		conf := config.GetConfig("config.yaml")

		//create mux (HTTP requests multiplier) which matches incoming requests
		// to existing routes
		route := mux.NewRouter()
		//create new route with GET method which gives all routes in JSON format
		route.HandleFunc("/routes/trains/", handlers.GetRoutesForTrains).Methods(
			"GET")
		route.HandleFunc("/routes/trains/{id}", handlers.GerTrainByID).
			Methods("GET")

		log.Fatal(http.ListenAndServe(conf.GetPort(), route))

	},
}

func init() {
	rootCmd.AddCommand(runCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// runCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
