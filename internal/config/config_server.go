package config

import (
	"io/ioutil"
	"log"
	"gopkg.in/yaml.v2"
	"strconv"
	"fmt"
)

type Configuration interface{
	GetPort() string
	GetTrain() string
	GetLogFile() string
}

type configuration struct{
	Server server `yaml:"server"`
	Train trains `yaml:"trains"`
	Log logFile `yaml:"log"`

}

type logFile struct{
	LogFile string `yaml:"log_file"`
}

type server struct{
	Port int `yaml:"port"`
}

type trains struct{
	DatasetFilename string `yaml:"dataset_filename"`
}

func (conf *configuration) GetLogFile() string{
	return conf.Log.LogFile
}

func (conf *configuration) GetPort() string{
	var port string

	if conf.Server.Port == 0 || conf.Server.Port < 8000 || conf.Server.Port > 8500{
		fmt.Println("Incorrect port, ", conf.Server.Port,
			"use only port from 8000 to 8500 in config.yaml file.")
	}

	port = ":" + strconv.Itoa(conf.Server.Port)
	fmt.Println("Server uses, this:", port, "port.")

	return port
}

func (conf *configuration) GetTrain() string {
	return conf.Train.DatasetFilename
}

func GetConfig(path string) Configuration{
	var conf configuration

	//read config.yaml file
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil{
		log.Println("Error in opening config.yaml file: ", err)
	}
	//decode config.yaml file
	err = yaml.Unmarshal(yamlFile, &conf)
	if err != nil{
		log.Println("Error in unmarshal config.yaml file: ", err)
	}

	return &configuration{conf.Server, conf.Train, conf.Log}
}