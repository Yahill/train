package test_config

import (
	"testing"
	"github.com/spf13/train/internal/config"
)

func TestConfig(t *testing.T){
	cf := config.GetConfig("config_test.yaml")

	if cf.GetPort() != ":8080"{
		t.Error(
			"For", "Read config, reading port.",
			"Expected", ":8080",
			"Got", cf.GetPort(),
			)
	}

	if cf.GetTrain() != "data.xml"{
		t.Error(
			"For", "Read config, reading dataset path.",
			"Expected", "data.xml",
			"Got", cf.GetTrain(),
			)
	}
}