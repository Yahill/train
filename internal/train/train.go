package train

import (
"os"
"fmt"
"io/ioutil"
"encoding/xml"
"github.com/spf13/train/internal/config"
	"strconv"
)

type Trains interface{
	AllStations() *trains
	GetTrainByID(id string) (train, error)
}

type route struct{
	DepartureStationID int
	ArrivalStationId int
}

type trains struct {
	Train [] train `xml:"TrainLeg"`
}

type train struct {
	TrainId             int     `xml:"TrainId,attr"`
	DepartureStationId  int     `xml:"DepartureStationId,attr"`
	ArrivalStationId    int     `xml:"ArrivalStationId,attr"`
	Price               float64 `xml:"Price,attr"`
	ArrivalTimeString   string  `xml:"ArrivalTimeString,attr"`
	DepartureTimeString string  `xml:"DepartureTimeString,attr"`
	Route 				[]int
}

type errTrainId string

func LoadTrains() Trains{
	trains := &trains{}
	dataSet := config.GetConfig("config.yaml")

	//this function loads info about trains from .xml file
	xmlFIle, err := os.Open(dataSet.GetTrain())
	if err != nil{
		fmt.Println(err)
	}
	defer xmlFIle.Close()

	byteValue, err := ioutil.ReadAll(xmlFIle)
	if err != nil{
		fmt.Println(err)
	}

	err = xml.Unmarshal(byteValue, trains)
	if err != nil{
		fmt.Printf("error: %v", err)
	}

	return trains
}

func (tr *trains) AllStations() *trains{
	nodes := createGraph(tr)
	tr.shortestWaysTrains(nodes)

	return tr
}

func (tr *trains) GetTrainByID(id string) (train, error){
	var result train

	for i := range tr.Train{
		if id == strconv.Itoa(tr.Train[i].TrainId){
			result = tr.Train[i]
			return result, nil
		}
	}
	return result, errTrainId(id)
}

func (e errTrainId) Error() string{
	return fmt.Sprintf("There is no train with such ID: %v", string(e))
}

func (tr *trains) shortestWaysTrains(nodes map[int][]int){
	//method for creation of shortest ways for the each train

	//slice with all unic stations
	var stations []int

	//getting all unic stations from the graph
	for key := range nodes{
		stations = append(stations, key)
	}

	//create route with maximum stations for each train
	for j := range tr.Train {
		var buff []int
		buff = createRoute(tr.Train[j].DepartureStationId, stations[1], nodes)

		for i := 0; i < len(stations) - 1; i++ {
			buffNext := createRoute(tr.Train[j].DepartureStationId,
				stations[i+1], nodes)
			if len(buff) <= len(buffNext) {
				buff = buffNext
				tr.Train[j].Route = buffNext
			}
		}
	}
}

func createGraph(tr *trains) map[int][]int {
	//searching for unic stations

	//map with visited stations
	encountered := map[route]bool{}
	//structure with arrivalStationId and departureStationId
	var allStations []route
	//structure with all unic stations
	var result []route
	//create map
	var graph map[int][]int
	graph = make(map[int][]int)

	//create first structure with all the stations
	for i := 0; i < len(tr.Train); i++ {
		var buff route

		buff.DepartureStationID = tr.Train[i].DepartureStationId
		buff.ArrivalStationId = tr.Train[i].ArrivalStationId

		allStations = append(allStations, buff)
	}

	//adding slice with unic routes
	for v := range allStations {
		if encountered[allStations[v]] == true {
			//do not add duplicate
		} else {
			//record this element to encountered
			encountered[allStations[v]] = true
			buff := allStations[v]
			//append to result slice
			result = append(result, buff)
		}
	}

	//create graph from unic stations
	for i := range result {
		graph[result[i].DepartureStationID] = append(graph[result[i].DepartureStationID],
			result[i].ArrivalStationId)
	}

	return graph
}

func depthFirstSearch(node int, nodes map[int][]int, fn func(int)) {
	//recursive with deapth-first search
	recursive(node, map[int]bool{}, fn, nodes)
}

func recursive(node int, currentStation map[int]bool, fn func(int),
	nodes map[int][]int) {
	//putting station to the map so we can understand that we have visited it
	currentStation[node] = true
	fn(node)
	//checking from all graph elements every edge
	for _, n := range nodes[node] {
		//we have edge to the next station and it is not in map create recursion
		if _, ok := currentStation[n]; !ok {
			recursive(n, currentStation, fn, nodes)
		}
	}
}

func createRoute(departureStation, arrivalStation int,
	nodes map[int][]int) []int {
	//route which will contain ready route for the passenger
	var route []int
	//route from departure station to all other
	allRouteFromDepartureStation := []int{}

	//count whole route from graph
	depthFirstSearch(departureStation, nodes, func(node int) {
		allRouteFromDepartureStation = append(allRouteFromDepartureStation, node)
	})
	//create route to the arrival station
	for i := range allRouteFromDepartureStation {
		if allRouteFromDepartureStation[i] != arrivalStation {
			buff := allRouteFromDepartureStation[i]
			route = append(route, buff)
		} else {
			buff := allRouteFromDepartureStation[i]
			route = append(route, buff)
			break
		}
	}
	return route
}