package logger

import (
	logger "github.com/Sirupsen/logrus"
	"os"
	"fmt"
	"github.com/spf13/train/internal/config"
)

func Info(args interface{}){
	filePath := config.GetConfig("config.yaml")

	f, err := os.OpenFile(filePath.GetLogFile(), os.O_WRONLY | os.O_APPEND | os.
		O_CREATE, 0644)

	Formatter := new(logger.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	logger.SetFormatter(Formatter)

	if err != nil {
		// Cannot open log file. Logging to stderr
		fmt.Println(err)
	}else{
		logger.SetOutput(f)
	}

	logger.Info(args)
}

func Warning(args interface{}){
	filePath := config.GetConfig("config.yaml")

	f, err := os.OpenFile(filePath.GetLogFile(), os.O_WRONLY | os.O_APPEND | os.
		O_CREATE, 0644)

	Formatter := new(logger.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	logger.SetFormatter(Formatter)

	if err != nil {
		// Cannot open log file. Logging to stderr
		fmt.Println(err)
	}else{
		logger.SetOutput(f)
	}

	logger.Warning(args)
}
